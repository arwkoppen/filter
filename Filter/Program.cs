﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.IO;
using static System.Math;

namespace Filter
{
    public class Program
    {

        public static void Main(string[] args)
        {
            var sw = new double[256, 3];
            var host = new Microsoft.AspNetCore.Hosting.WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();
            double del=0, f=0, c1, c2, eps, help, ks, kw;
            string s;
            int k, n = 0;
            bool canConvert=false;
            Console.WriteLine("\nEnter the ripple in dB (delta): ");
            while (canConvert == false) 
            {
                s = Console.ReadLine();
                canConvert = double.TryParse(s, out del);
            }
            eps = Sqrt( Pow (10.0 , y: (0.1 * del)) - 1);
            Console.WriteLine("Enter the order of the filter (n): ");
            s = Console.ReadLine();
            canConvert = false;
            while (canConvert == false)
            {
                s = Console.ReadLine();
                canConvert = int.TryParse(s, out n);
            }
            help = 1 / eps;
            help = help + Sqrt((help * help) + 1);
            help = Pow(help, (1 / n));
            ks = (help - 1 / help) / -2;
            kw = (help + 1 / help) / 2;
            Console.WriteLine("\neps: {0}; ks: {1}; kw: {2}\n\n", eps, ks, kw);
            for (k = 0; k < n; k++)
            {
                help = PI * (1 + 2 * k) / (2 * n);
                sw[k + 1, 1] = ks * Sin(help);
                sw[k + 1,2] = kw * Cos(help);
                Console.WriteLine("Pole {0} @: sigma: {1}; omega: {2}\n", (k + 1), sw[k + 1,1], sw[k + 1,2]);
            }
            Console.WriteLine("Enter the -3dB point: ");
            canConvert = false;
            while (canConvert == false)
            {
                s = Console.ReadLine();
                canConvert = double.TryParse(s, out f);
            }
            Console.WriteLine("\nThe capacitor-values are normalised for 1000 Ohm: ");
            for (k = (int)((n - 1) / 2); k >= 0; k--)
            {
                c1 = (-1 / sw[k + 1,1]);
                if (sw[k + 1,2] > 1e-6)
                {
                    c2 = (1 / (sw[k + 1,1] * sw[k + 1,1] + sw[k+1,2]* sw[k + 1,2]) * c1);
                }
                else c2 = 0;
                Console.WriteLine("C{0}t= {1:E}, C{2}, n= {3:E}\n", (k + 1), (c1 / f / 2e3 / PI), (k + 1), (c2 / f / 2e3 / PI));
            }
            Console.WriteLine("Press Enter to end program");
            s=Console.ReadLine();
        }

    }
}
